(function() {
    'use strict';

    angular
        .module('findjobs')
        .config(function($stateProvider, $urlRouterProvider) {

          // Ionic uses AngularUI Router which uses the concept of states
          // Learn more here: https://github.com/angular-ui/ui-router
          // Set up the various states which the app can be in.
          // Each state's controller can be found in controllers.js
          $stateProvider

        // setup an abstract state for the tabs directive
          .state('tab', {
          url: '/tab',
          abstract: true,
          templateUrl: 'views/tabs.html'
        })

        // Each tab has its own nav history stack:

        .state('tab.search', {
          url: '/search',
          views: {
            'tab-search': {
              templateUrl: 'views/tab-search.html',
              controller: 'SearchCtrl as vm'
            }
          }
        })

        .state('tab.favorite', {
            url: '/favorite',
            views: {
              'tab-favorite': {
                templateUrl: 'views/tab-favorite.html',
                controller: 'FavoriteCtrl as vm'
              }
            }
          })

          .state('tab.jobs', {
            url: '/jobs/:idState/:idArea',
            views: {
              'tab-search': {
                templateUrl: 'views/jobs.html',
                controller: 'JobsCtrl as vm'
              }
            }
          })

          .state('tab.jobs-detail', {
            url: '/jobs/:id',
            views: {
              'tab-search': {
                templateUrl: 'views/jobs-detail.html',
                controller: 'JobsDetailCtrl as vm'
              }
            }
          })

        .state('tab.preferences', {
          url: '/preferences',
          views: {
            'tab-preferences': {
              templateUrl: 'views/tab-preferences.html',
              controller: 'PreferencesCtrl as vm'
            }
          }
        });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/tab/search');

    });

})();