(function() {
    'use strict';

    angular
        .module('findjobs')
        .constant('constants', {

            'APP_NAME': 'FindJobs',


            /////////////////////

            'STATE_JOBS': 'tab.jobs'

        })

})();