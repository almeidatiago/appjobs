(function() {
    'use strict';

    angular
        .module('findjobs')
        .controller('JobsCtrl', JobsCtrl);

    /* @ngInject */
    function JobsCtrl(constants) {
        var vm = this;
        vm.title = 'Vagas';
        vm.vagas = [];

        activate();

        ////////////////

        function activate() {

            vm.vagas = [
                {
                    id: 1,
                    nome: 'Vendedor',
                    nomeEmpresa: 'Insinuante',
                    cidade: 'Salvador',
                    siglaEstado: 'BA'
                },
                {
                    id: 2,
                    nome: 'Analista de Sistema',
                    nomeEmpresa: 'Intel Software',
                    cidade: 'São Paulo',
                    siglaEstado: 'SP'
                },
                {
                    id: 3,
                    nome: 'Vendedor',
                    nomeEmpresa: 'Insinuante',
                    cidade: 'Salvador',
                    siglaEstado: 'BA'
                },
                {
                    id: 4,
                    nome: 'Analista de Sistema',
                    nomeEmpresa: 'Intel Software',
                    cidade: 'São Paulo',
                    siglaEstado: 'SP'
                },
                {
                    id: 5,
                    nome: 'Vendedor',
                    nomeEmpresa: 'Insinuante',
                    cidade: 'Salvador',
                    siglaEstado: 'BA'
                },
                {
                    id: 6,
                    nome: 'Analista de Sistema',
                    nomeEmpresa: 'Intel Software',
                    cidade: 'São Paulo',
                    siglaEstado: 'SP'
                }
            ];
        }
    }
})();