(function() {
    'use strict';

    angular
        .module('findjobs')
        .controller('SearchCtrl', SearchCtrl);

    /* @ngInject */
    function SearchCtrl(constants, $state) {
        var vm = this;
        vm.title = constants.APP_NAME;
        vm.subtitle = "Procure uma vaga"
        vm.search = search;

        activate();

        ////////////////

        function activate() {
        }

        function search() {
            console.log('Redirecionando...');
            $state.go(constants.STATE_JOBS, {idState: 1, idArea: 2});
        }
    }
})();