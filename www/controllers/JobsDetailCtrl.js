(function() {
    'use strict';

    angular
        .module('findjobs')
        .controller('JobsDetailCtrl', JobsDetailCtrl);

    /* @ngInject */
    function JobsDetailCtrl(constants) {
        var vm = this;
        
        vm.vaga = [];
        vm.title = '';

        activate();

        ////////////////

        function activate() {

            vm.vaga = {
                id: 1,
                nome: 'Analista de Sistemas',
                empresa: 'Aolutis Tecnologias',
                especialidade: 'Tecnologia da Informação',
                tipoRegime: 'CLT',
                remuneracao: 'R$ 5.500,00',
                cargaHoraria: '40',
                estado: 'Bahia',
                exigencias: ['Certificação Java', 'Superio Completo TI']
            }

            vm.title = vm.vaga.nome;
        }
    }
})();