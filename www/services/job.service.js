(function() {
    'use strict';

    angular
        .module('findjobs')
        .factory('JobService', JobService);

    /* @ngInject */
    function JobService() {
        var service = {
            search: search
        };
        return service;

        ////////////////

        function search(idState, idArea) {
        	return [{'id': 312, 'empresa': 'Empresa 312312', 'area' : 'TI', 'estado': 'Bahia', 'descricao': 'LoL'}, 
        	{'id': 15312, 'empresa': 'Empresa 123', 'area' : 'TI', 'estado': 'Bahia', 'descricao': 'LoL123'},
        	{'id': 312, 'empresa': 'Empresa wrew', 'area' : 'TI', 'estado': 'Bahia', 'descricao': 'LoLeqw'},
        	{'id': 123, 'empresa': 'Empresa fsd', 'area' : 'TI', 'estado': 'Bahia', 'descricao': 'LoLdasdSA'}];
        }
    }
})();